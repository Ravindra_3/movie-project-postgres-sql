const express = require("express")
const app = express()
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
require("dotenv/config")


const {
    Pool
} = require('pg')

const pool = new Pool()
var x = require("./director.js")
const y = require("./movies.js")


app.get("/api/directors", x.getdirector)
app.get("/api/directors/:directorId", x.getdirectorbyid)
app.post("/api/directors", x.adddirector)
app.put("/api/directors/:directorId", x.updatedirector)
app.delete("/api/directors/:directorId", x.deletdirector)


app.get("/api/movies/", y.getmovies)
app.get('/api/movies/:movieId', y.getmoviesbyid)
app.put("/api/movies/:movieId", y.updamoviebyid)
app.delete("/api/movies/:movieId", y.deletemoviebyid)
app.post("/api/movies", y.addmovie)



app.listen(3000, () => console.log('listinig on port 3000'))