const {createLogger,format,transports}=require("winston")
module.exports=createLogger({

    format:format.combine(format.simple(),
    format.timestamp(),
    format.printf(info=>`[${info.timestamp}] ${info.level} ${info.message}`
    )),

    // format:format.combine(format.simple()),
    transports:[
        new transports.File({
         maxsize:51000,
         maxFiles:5,
         filename:"./log-api.log",
         format:format.combine()
        }),
        new transports.Console({
          level:'debug',
          format:format.combine(format.simple())
        })
    ]
})